package com.learning.portfolio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PersonalFinancialGoalServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PersonalFinancialGoalServiceApplication.class, args);
	}

}
